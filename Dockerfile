FROM ubuntu:22.04

# Install Packages, via apt-get. 
ENV DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC

RUN apt-get update && apt-get install -y \
	wget \
    rsync \
    moreutils \
    texlive-latex-base texlive-fonts-recommended texlive-latex-extra texlive-fonts-extra
RUN wget -q https://github.com/jgm/pandoc/releases/download/2.19/pandoc-2.19-1-amd64.deb \
 && apt install -y ./pandoc-2.19-1-amd64.deb \
  && rm -f ./pandoc-2.19-1-amd64.deb
RUN apt install -y python3-pip \
 && pip3 install --user pandoc-include pandoc-acro
ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/root/.local/bin
## Eisvogel
ARG TMPL_URL=https://github.com/Wandmalfarbe/pandoc-latex-template/releases/download
ARG TMPL_VER=2.0.0
WORKDIR /usr/local/src/eisvogel
RUN wget -qO- ${TMPL_URL}/v${TMPL_VER}/Eisvogel-${TMPL_VER}.tar.gz |tar xfz -
RUN wget -qO /usr/local/bin/pandoc-gls.lua \
 https://raw.githubusercontent.com/tomncooper/pandoc-gls/master/pandoc-gls.lua \
 && chmod +x /usr/local/bin/pandoc-gls.lua
VOLUME /data
WORKDIR /data